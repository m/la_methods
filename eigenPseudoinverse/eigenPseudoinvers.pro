TEMPLATE = app
CONFIG += console c++17
CONFIG -= app_bundle
CONFIG -= qt
INCLUDEPATH += "/usr/include/eigen3/"

SOURCES += \
        main.cpp
