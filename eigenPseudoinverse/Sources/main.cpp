#include <iostream>
#include <eigen3/Eigen/Eigen>
#include <eigen3/Eigen/QR>

//it's a lambda func for non-square matrix inverse using pseudoInverse method completeOrthogonalDecomposition()
auto pinv_method1 = [] ( auto matrix) {
        return (matrix.completeOrthogonalDecomposition().pseudoInverse());
};

//it's a lambda func for non-square matrix inverse using pseudoInverse method moore-penrose
//left inverse
auto pinv_method2 = [] (auto matrix){
        return ( ( (matrix.transpose()*matrix).inverse() ) * matrix.transpose() );
};

//it's a lambda func for non-square matrix inverse using pseudoInverse method moore-penrose
//right inverse
auto pinv_method3 = [] (auto matrix){
        return ( matrix.transpose() * ( (matrix*matrix.transpose()).inverse() ) );
};



int main()
{
    //left inverse matrices
    Eigen::MatrixXf A0 = Eigen::MatrixXf(4,2);
    A0 << 3.0f,9.2f,
          1.4f,8.2f,
          5.0f,6.3f,
          1.8f,4.7f;

    std::cout << "Here is a 4x4 float matrix, A0:" << std::endl <<  A0 << std::endl << std::endl;
    std::cout << "Here is a pseudoInverse 4x4 float matrix, A0:" << std::endl << pinv_method1(A0) << std::endl << std::endl;

    Eigen::MatrixXd A1 = Eigen::MatrixXd(4,2);
    A1 << 3.0,9.2,
          1.4,8.2,
          5.0,6.3,
          1.8,4.7;


    std::cout << "Here is a 4x4 double matrix, A1:" << std::endl << A1 << std::endl << std::endl;
    std::cout << "Here is a pseudoInverse 4x4 double matrix, A1:" << std::endl << pinv_method1(A1) << std::endl << std::endl;

    Eigen::MatrixXf A2 = Eigen::MatrixXf(4,2);
    A2 << 3.0f,9.2f,
          1.4f,8.2f,
          5.0f,6.3f,
          1.8f,4.7f;


    std::cout << "Here is a 4x4 float matrix, A2:" << std::endl <<  A2 << std::endl << std::endl;
    std::cout << "Here is a pseudoInverse 4x4 float matrix, A2:" << std::endl << pinv_method2(A2) << std::endl << std::endl;

    Eigen::MatrixXd A3 = Eigen::MatrixXd(4,2);
    A3 << 3.0,9.2,
          1.4,8.2,
          5.0,6.3,
          1.8,4.7;


    std::cout << "Here is a 4x4 double matrix, A3:" << std::endl << A3 << std::endl << std::endl;
    std::cout << "Here is a pseudoInverse 4x4 double matrix, A3:" << std::endl << pinv_method2(A3) << std::endl << std::endl;


    //right inverse matrices

    Eigen::MatrixXf A4 = Eigen::MatrixXf(2,4);
    A4 << 3.0f,1.4f,5.0f,1.8f,
          9.2f,8.2f,6.3f,4.7f;


    std::cout << "Here is a 4x4 float matrix, A4:" << std::endl <<  A4 << std::endl << std::endl;
    std::cout << "Here is a pseudoInverse 4x4 float matrix, A4:" << std::endl << pinv_method1(A4) << std::endl << std::endl;

    Eigen::MatrixXd A5 = Eigen::MatrixXd(2,4);
    A5 << 3.0,1.4,5.0,1.8,
          9.2,8.2,6.3,4.7;

    std::cout << "Here is a 4x4 double matrix, A5:" << std::endl << A5 << std::endl << std::endl;
    std::cout << "Here is a pseudoInverse 4x4 double matrix, A5:" << std::endl << pinv_method1(A5) << std::endl << std::endl;

    Eigen::MatrixXf A6 = Eigen::MatrixXf(2,4);
    A6 << 3.0f,1.4f,5.0f,1.8f,
          9.2f,8.2f,6.3f,4.7f;

    std::cout << "Here is a 4x4 float matrix, A6:" << std::endl <<  A6 << std::endl << std::endl;
    std::cout << "Here is a pseudoInverse 4x4 float matrix, A6:" << std::endl << pinv_method3(A6) << std::endl << std::endl;

    Eigen::MatrixXd A7 = Eigen::MatrixXd(2,4);
    A7 << 3.0,1.4,5.0,1.8,
          9.2,8.2,6.3,4.7;

    std::cout << "Here is a 4x4 double matrix, A7:" << std::endl << A7 << std::endl << std::endl;
    std::cout << "Here is a pseudoInverse 4x4 double matrix, A7:" << std::endl << pinv_method3(A7) << std::endl << std::endl;

    return 0;
}
